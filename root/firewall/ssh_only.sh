#!/bin/sh
### Flush rules
iptables -F
iptables -X

### Default policies
iptables -P OUTPUT DROP
iptables -P INPUT DROP
iptables -P FORWARD DROP

### Established connections
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

### LOOPBACK
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

#DNS
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT

### SSH
iptables -A INPUT -p tcp --dport 3022 -m state --state NEW -m recent --name DOS_SSH --set
iptables -A INPUT -p tcp --dport 3022 -m state --state NEW -m recent --name DOS_SSH --update --hitcount 20 --seconds 30 -j LOG --log-prefix "DOS_SSH"
iptables -A INPUT -p tcp --dport 3022 -m state --state NEW -m recent --name DOS_SSH --update --hitcount 20 --seconds 30 -j DROP
iptables -A INPUT -p tcp --dport 3022 -j ACCEPT

iptables -A OUTPUT -p tcp --dport 3022 -j ACCEPT
