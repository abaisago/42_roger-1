#!/bin/bash

old_sum="$(cat /var/cache/root/crontab.md5)";
new_sum="$(md5sum /etc/crontab)";
if [[ $old_sum != $new_sum ]]; then
	echo "/etc/crontab has been modified on $(date)" | mail -s "CRONTAB MODIFIED" root && echo "$new_sum" > /var/cache/root/crontab.md5
fi
