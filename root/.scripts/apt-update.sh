echo "$(date)" > /var/log/update_script.log
echo "------------------------------------ \n" >> /var/log/update_script.log
apt -y update 2>/dev/null >> /var/log/update_script.log
echo "\n------------------------------------\n" >> /var/log/update_script.log
apt -y upgrade 2>/dev/null >> /var/log/update_script.log
